import logging
from config.config import config_scrapping
from const import START_LOG, END_LOG

FILEPATH = config_scrapping.get("log", "filepath")
logFormatter = logging.Formatter("%(asctime)s %(name)-10s %(levelname)-8s %(message)s")
rootLogger = logging.getLogger(__name__)
fileHandler = logging.FileHandler(FILEPATH)
consoleHandler = logging.StreamHandler()

rootLogger.setLevel(logging.INFO)

fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)


def start_end_decorator(func):
    def wrapper(*args, **kwargs):
        rootLogger.info(f"{START_LOG} : {func.__name__}()")
        func_ = func(*args, **kwargs)
        rootLogger.info(f"{END_LOG} : {func.__name__}()")
        return func_

    return wrapper
