# Recommander-NLP

## Introduction

This project is based on Europe1's articles. It's a content-based recommender. The first step is to get the data. Therefore, we have to scrap gently Europe1 website. 

## Presentation

Extract articles links -> Extract articles contents -> Use articles contents to train ML module -> Every article have 10 articles similar

## Installation

1. Clone the project

2. Take a look at /config/config.ini and change values if you need

3. Launch the scrapper : `python scrapping/main_scrapping.py`

4. Launch the pre processing : `python recommenders/pre_processing/main_preprocessing.py`

5. Launch the recommender based on CountVectorizer (https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html) :
 `python recommenders/main_recommenders.py`

## TODO (prioritized) : 

- Dockerize the projet
- Use airflow to launch every part of the project every day
- Write Docstring
- Write more and check all tests 
