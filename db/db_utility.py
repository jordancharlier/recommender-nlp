import pandas as pd

from const import TICK_PROGRESS, COLUMNS_NAMES
from db.db_connection import DataBaseConnection
from log.log_conf import rootLogger, start_end_decorator


class DataBaseUtility:
    @staticmethod
    @start_end_decorator
    def initiate_schema_database(column_names):
        df_database = pd.DataFrame(columns=column_names,)
        return df_database

    @staticmethod
    @start_end_decorator
    def initiate_id_database(df_database, list_urls):
        df_database["url"] = list_urls
        df_database = df_database.fillna(0)
        return df_database

    @staticmethod
    def update_document(collection, document, data_to_update):
        collection.update_one({"_id": document["_id"]}, {"$set": data_to_update})

    @staticmethod
    @start_end_decorator
    def insert_df_to_db(df_database):
        collection = DataBaseConnection.getInstance()
        dict_articles = df_database.to_dict(orient="records")
        collection.insert_many(dict_articles)

    @staticmethod
    def delete_document(url, collection):
        id_document = {"url": url}
        collection.delete_one(id_document)
        rootLogger.warning(f"get_articles_europe1() : The URL {url} is not valid")

    @staticmethod
    def get_progession(index, number_of_in_procress):
        if index % TICK_PROGRESS == 0:
            rootLogger.info(f"{index}/{number_of_in_procress}")

    @staticmethod
    def db_data_to_df(query_document_to_process, collection):
        data_to_process = collection.find(query_document_to_process)
        df_database = pd.DataFrame(list(data_to_process), columns=COLUMNS_NAMES)
        df_database = df_database.set_index("url")

        return df_database

    @staticmethod
    @start_end_decorator
    def insert_df_in_db(query_document_to_process, df_database, collection):
        data_to_process = collection.find(query_document_to_process)

        for doc_to_update, row in zip(data_to_process, df_database.iterrows()):
            DataBaseUtility.update_document(collection, doc_to_update, row[1].to_dict())
