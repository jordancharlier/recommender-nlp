from pymongo import MongoClient

from config.config import config_scrapping
from log.log_conf import start_end_decorator


class DataBaseConnection:
    host = config_scrapping.get("database", "host")
    port = config_scrapping.getint("database", "port")
    collection = config_scrapping.get("database", "collection")

    client = MongoClient(host=host, port=port)
    db = client[collection]
    collection = db[collection]
    __shared_instance_collection = collection

    @staticmethod
    @start_end_decorator
    def getInstance():
        if DataBaseConnection.__shared_instance_collection is None:
            DataBaseConnection()
        return DataBaseConnection.__shared_instance_collection
