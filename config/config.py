from configparser import ConfigParser
import os

config_scrapping = ConfigParser()
config_scrapping.read(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "config_scrapping.ini")
)

config_recommender = ConfigParser()
config_recommender.read(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "config_recommender.ini")
)
