from fake_useragent import UserAgent

ua = UserAgent()
LIST_HEADERS = [
    {"User-Agent": str(ua.chrome)},
    {"User-Agent": str(ua.firefox)},
    {"User-Agent": str(ua.opera)},
    {"User-Agent": str(ua.ie)},
    {"User-Agent": str(ua.google)},
    {"User-Agent": str(ua.safari)},
]
PROXIES = {"http": "socks5://127.0.0.1:9050", "https": "socks5://127.0.0.1:9050"}
IP_EXPIRATION = 20
LIST_URL_TO_SCRAP = [
    "https://www.europe1.fr/politique",
    "https://www.europe1.fr/sport",
    "https://www.europe1.fr/medias-tele",
    "https://www.europe1.fr/culture",
    "https://www.europe1.fr/economie",
    "https://www.europe1.fr/societe",
    "https://www.europe1.fr/faits-divers",
    "https://www.europe1.fr/sante",
]

COLUMNS_NAMES = [
    "url",
    "html",
    "raw_title",
    "raw_chapo",
    "raw_text",
    "clean_title",
    "clean_chapo",
    "clean_text",
    "timestamp",
    "field",
    "status",
    "similar_urls",
]

START_LOG = "Start of"
END_LOG = "End of"
TICK_PROGRESS = 1
COLUMNS_TO_CLEAN = {
    "raw_title": "clean_title",
    "raw_chapo": "clean_chapo",
    "raw_text": "clean_text",
}
