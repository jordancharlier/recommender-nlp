import random
import time

import requests
from bs4 import BeautifulSoup
from stem import Signal
from stem.control import Controller

from const import IP_EXPIRATION, PROXIES, LIST_HEADERS, LIST_URL_TO_SCRAP
from log.log_conf import start_end_decorator


def get_html(url_to_scrap, index, headers):
    if index % IP_EXPIRATION == 0:
        change_ip()
    r = requests.get(url=url_to_scrap, headers=headers, proxies=PROXIES)
    soup = BeautifulSoup(r.text, "html.parser")
    return soup


def change_ip():
    """Changing IP adress with tor"""
    with Controller.from_port(port=9051) as c:
        c.authenticate()
        c.signal(Signal.NEWNYM)


def navigate_incognito():
    # if index % IP_EXPIRATION == 0:
    headers = random.choice(LIST_HEADERS)
    change_ip()
    return headers


@start_end_decorator
def remove_duplicate_links(list_links):
    list_links = list(set(list_links))
    return list_links


def scrap_politetly(url_to_scrap, index, extract_content_html, headers):
    soup = get_html(url_to_scrap, index, headers)
    data_from_html = extract_content_html(url_to_scrap, soup)
    return data_from_html


@start_end_decorator
def get_links_on_page(extract_links_articles):
    headers = navigate_incognito()

    list_pages = list(range(50, 54))
    list_links = []
    for url_to_scrap in LIST_URL_TO_SCRAP:

        for page in list_pages:
            if page % IP_EXPIRATION == 0:
                headers = navigate_incognito()

            url_to_scrap_page = "{}/{}".format(url_to_scrap, page)
            soup = get_html(url_to_scrap_page, page, headers)
            list_links.extend(extract_links_articles(soup))

        time.sleep(random.uniform(0.1, 2))
    return list_links
