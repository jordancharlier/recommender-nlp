import sys
import os
from pathlib import Path

sys.path.insert(0, Path(os.path.abspath(__file__)).parents[1])

from const import COLUMNS_NAMES
from db.db_connection import DataBaseConnection
from db.db_utility import DataBaseUtility
from scrapping.scrapping_europe1 import (
    get_articles_europe1,
    extract_links_europe1,
    extract_data_from_europe1,
)
from scrapping.scrapping_utils import get_links_on_page, remove_duplicate_links

if __name__ == "__main__":
    DataBaseConnection()
    list_links = get_links_on_page(extract_links_europe1)
    list_links = remove_duplicate_links(list_links)
    df_database = DataBaseUtility.initiate_schema_database(COLUMNS_NAMES)
    df_database = DataBaseUtility.initiate_id_database(df_database, list_links)
    DataBaseUtility.insert_df_to_db(df_database,)
    get_articles_europe1(extract_data_from_europe1)
