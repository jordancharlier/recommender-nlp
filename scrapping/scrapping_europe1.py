import random
import time

from dateutil import parser
from db.db_connection import DataBaseConnection
from db.db_utility import DataBaseUtility
from const import IP_EXPIRATION
from log.log_conf import rootLogger, start_end_decorator
from scrapping.scrapping_utils import (
    scrap_politetly,
    navigate_incognito,
)


@start_end_decorator
def get_articles_europe1(extract_data_from_europe1):

    collection = DataBaseConnection.getInstance()
    query_document_to_process = {"status": 0}
    document_to_process = collection.find(query_document_to_process)
    number_of_in_procress = collection.count_documents(query_document_to_process)
    for index, document in enumerate(document_to_process):
        time.sleep(random.uniform(0.1, 1.2))  # To not flood the website
        DataBaseUtility.get_progession(index, number_of_in_procress)
        if index % IP_EXPIRATION == 0:
            headers = navigate_incognito()

        if document["status"] == 0:
            url_to_scrap = document["url"]
            data_from_html = scrap_politetly(
                url_to_scrap, index, extract_data_from_europe1, headers
            )
            if data_from_html == 0:
                DataBaseUtility.delete_document(url_to_scrap, collection)
                rootLogger.info(
                    f"{url_to_scrap} is a bad link : {get_articles_europe1.__name__}()"
                )
            else:
                DataBaseUtility.update_document(collection, document, data_from_html)


def extract_links_europe1(soup):
    list_links = []
    for div in soup.find_all("div", {"class": "bloc"}):
        for a in div.find_all("a"):
            list_links.append(a["href"])
    return list_links


def extract_data_from_europe1(url_to_scrap, soup):
    try:
        title = soup.find("title").text
        chapo = soup.find("strong", {"class": "chapo"}).text
        section = soup.find("section", {"class": "content"})
        datatime_ = soup.find("time")["datetime"]
        datatime_ = parser.parse(datatime_)
        timestamp = datatime_.timestamp()
        field = url_to_scrap.split("/")[3]
        text = []
        for p in section.find_all("p"):
            text.append(p.text)
        text = " ".join(text)
        data_from_html = {
            "html": str(soup),
            "raw_title": title,
            "raw_chapo": chapo,
            "raw_text": text,
            "timestamp": timestamp,
            "field": field,
            "status": 1,
        }

    except AttributeError as e:
        rootLogger.error(e, exc_info=True)
        return 0
    return data_from_html
