import sys
import os
from pathlib import Path

sys.path.insert(0, Path(os.path.abspath(__file__)).parents[1])

from recommenders.counter import CountRecommender

if __name__ == "__main__":
    count_recommender = CountRecommender()
    count_recommender.load_corpus()
    count_recommender.fit_transform()
    count_recommender.predict(count_recommender.corpus)
