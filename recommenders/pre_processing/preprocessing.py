from nltk import WordNetLemmatizer, word_tokenize
from nltk.stem.snowball import FrenchStemmer
from spacy.lang.fr.stop_words import STOP_WORDS

from const import COLUMNS_TO_CLEAN
from log.log_conf import start_end_decorator


def flatten_list_in_series(column):
    column = column.str.join(" ")
    return column


def tokenizer(column):
    column = column.apply(lambda x: word_tokenize(x))
    return column


def remove_stop_words(column):
    stop = STOP_WORDS
    column = column.apply(lambda x: [word for word in x if word not in stop])
    return column


def remove_punctuations(column):
    column = column.apply(lambda x: [word for word in x if word.isalpha()])
    return column


def stemming(column):
    porter = FrenchStemmer()
    column = column.apply(lambda x: [porter.stem(word) for word in x])
    return column


def to_lowercase(column):
    """Convert all characters to lowercase from list of tokenized words"""
    column = column.apply(lambda x: [word.lower() for word in x])
    return column


def lemmatizing(column):
    lemmatizer = WordNetLemmatizer()
    column = column.apply(lambda x: [lemmatizer.lemmatize(word) for word in x])
    return column


def remove_single_chars(column):
    column = column.apply(lambda x: [word for word in x if len(word) > 1])
    return column


@start_end_decorator
def preprocessing_text(df_database):
    for column_to_clean, column_cleaned in COLUMNS_TO_CLEAN.items():
        df_database[column_cleaned] = df_database[column_to_clean]
        for func_prepossessing in DICT_PREPROCESSING.values():
            df_database[column_cleaned] = func_prepossessing(
                df_database[column_cleaned]
            )

    return df_database


@start_end_decorator
def keeping_data_to_process(df_database):
    columns = ["clean_title", "clean_chapo", "clean_text", "timestamp", "field"]
    return df_database.reindex(columns=columns)


DICT_PREPROCESSING = {
    "tokenizer": tokenizer,
    "remove_single_chars": remove_single_chars,
    "remove_punctuations": remove_punctuations,
    "to_lowercase": to_lowercase,
    "remove_stop_words": remove_stop_words,
    "lemmatizing": lemmatizing,
}
