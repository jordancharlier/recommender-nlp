import sys
import os
from pathlib import Path

sys.path.insert(0, Path(os.path.abspath(__file__)).parents[2])


from recommenders.pre_processing.preprocessing import (
    preprocessing_text,
    keeping_data_to_process,
)
from db.db_connection import DataBaseConnection
from db.db_utility import DataBaseUtility

if __name__ == "__main__":
    collection = DataBaseConnection.getInstance()
    query_document_to_process = {"status": 1}
    df_database = DataBaseUtility.db_data_to_df(query_document_to_process, collection)
    df_database = preprocessing_text(df_database)
    df_database = keeping_data_to_process(df_database)
    df_database["status"] = 2
    DataBaseUtility.insert_df_in_db(query_document_to_process, df_database, collection)
