from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

from log.log_conf import start_end_decorator
from recommenders.recommenders import Recommenders
from db.db_utility import DataBaseUtility


class CountRecommender(Recommenders):
    def __init__(self):
        Recommenders.__init__(self)
        self.count_vectorizer = CountVectorizer(ngram_range=(1, 2))

    @start_end_decorator
    def load_corpus(self):
        self.corpus = DataBaseUtility.db_data_to_df(
            self.query_document_to_process, self.collection
        )

    def pre_processing(self):
        self.corpus.reset_index(level=0, inplace=True)
        self.corpus.rename(columns={"index": "url"}, inplace=True)
        self.corpus["clean_text"] = self.corpus["clean_text"].str.join(" ")

    @start_end_decorator
    def fit_transform(self):
        self.pre_processing()
        self.corpus_transform = self.count_vectorizer.fit_transform(
            self.corpus["clean_text"]
        )

    def predict(self, data):
        articles = self.count_vectorizer.transform(data["clean_text"])
        cos_similarity_count = cosine_similarity(articles, self.corpus_transform)
        results = np.argsort(np.squeeze(-cos_similarity_count), axis=1)
        for result in results:
            self.corpus["similar_urls"].loc[result[0]] = (
                self.corpus["url"].iloc[result].values.tolist()
            )
            self.corpus["status"].loc[result[0]] = 3
        DataBaseUtility.insert_df_in_db(
            self.query_document_to_process, self.corpus, self.collection
        )
