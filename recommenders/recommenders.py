#!/usr/bin/env python
# coding: utf-8
from db.db_connection import DataBaseConnection


class Recommenders(object):
    def __init__(self):
        self.collection = DataBaseConnection.getInstance()
        self.query_document_to_process = {"status": 2}

        self.corpus = None
        self.corpus_transform = None

    def load_corpus(self):
        pass

    def pre_processing(self):
        pass

    def fit_transform(self):
        pass

    def predict(self, data):
        pass

    def load_corpus_transform(self):
        pass
